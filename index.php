<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <?php session_start(); ?>

	<form method="POST" action="./server.php">
		Username: <input type="email" name="email" required>
		Password: <input type="password" name="password" required>
		<button type="submit">Login</button>
	</form>

	<?php if(isset($_SESSION['email'])): ?>

		<p>Hello, <?= $_SESSION['email']; ?></p>
        <button name="logout">Logout</button>

	<?php else: ?>

		<?php if(isset($_SESSION['login_error'])): ?>

			<p><?= $_SESSION['login_error']; ?></p>

		<?php endif;?>	

	<?php endif;?>

        <script src="" async defer></script>
    </body>
</html>